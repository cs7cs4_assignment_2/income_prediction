import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestClassifier,RandomForestRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.datasets import make_classification
from sklearn.linear_model import LinearRegression
from mpl_toolkits.mplot3d import Axes3D
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn import preprocessing
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import Imputer, StandardScaler, MinMaxScaler
from sklearn import utils
from sklearn import svm
from sklearn.model_selection import cross_val_score
import category_encoders as ce



def removeOutliners(dataframe,dataframe2):
    """ Removes all the instances which has an outlier variable. """
    quantile1 = dataframe.quantile(0.25)
    quantile3 = dataframe.quantile(0.75)
    iqr = quantile3 - quantile1
    fenceLow = quantile1 - 1.5 * iqr
    fenceHigh = quantile3 + 1.5 * iqr
    fenceOut = (dataframe > fenceLow) & (dataframe < fenceHigh)
    notOutliers = fenceOut[fenceOut.columns[0]]
    for i, col in enumerate(list(fenceOut.columns)):
        if (i == 0): continue
        notOutliers = notOutliers & fenceOut[col]
    return dataframe.loc[notOutliers],dataframe2.loc[notOutliers]

training_dataset = pd.read_csv(
    'dataset/tcd-ml-1920-group-income-train.csv')
test_dataset = pd.read_csv(
    'dataset/tcd-ml-1920-group-income-train.csv')
training_dataset = training_dataset.loc[:, [ 'Instance','Year of Record','Housing Situation','Crime Level in the City of Employement','Work Experience in Current Job [years]','Satisfation with employer','Gender','Age','Country','Size of City','Profession','University Degree','Wears Glasses','Hair Color','Body Height [cm]','Yearly Income in addition to Salary (e.g. Rental Income)','Total Yearly Income [EUR]]']]
test_dataset = test_dataset.loc [:, [ 'Instance','Year of Record','Housing Situation','Crime Level in the City of Employement','Work Experience in Current Job [years]','Satisfation with employer','Gender','Age','Country','Size of City','Profession','University Degree','Wears Glasses','Hair Color','Body Height [cm]','Yearly Income in addition to Salary (e.g. Rental Income)','Total Yearly Income [EUR]]']]
# train_df = training_dataset.drop(["Instance"], axis=1)
# test_df = test_dataset.drop(["Instance"], axis=1)



### train and test df initialized
nonnull = [ 'Instance','Year of Record','Housing Situation','Crime Level in the City of Employement','Work Experience in Current Job [years]','Satisfation with employer','Gender','Age','Country','Size of City','Profession','University Degree','Wears Glasses','Hair Color','Body Height [cm]','Yearly Income in addition to Salary (e.g. Rental Income)','Total Yearly Income [EUR]]']

#NA values filled
training_dataset = pd.get_dummies(training_dataset, drop_first=False)
training_dataset.fillna(training_dataset.mean(), axis=0,inplace=True)
train_df = training_dataset
## Filling up na values with idxmax of all values (Winnorize).
#for i in nonnull:
#    exec ("train_df[\"%s\"].fillna(train_df[\"%s\"].value_counts().idxmax(), inplace=True)" % (i,i))
test_df = test_datset



Y = train_df["Income in EUR"]
X = train_df.drop(['Income in EUR','Gender', 'Hair Color', 'Wears Glasses'], axis=1)
Xtst = test_df.drop(['Income','Gender', 'Hair Color', 'Wears Glasses'], axis=1)
## Ends the data pre-processing section

# Removes outliners
train_data= removeOutliners(pd.DataFrame(train_df))







X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size = 0.2, random_state = 0)
clf = svm.SVC(kernel='linear', C=1)

## K-fold cross validation
scores = cross_val_score(clf, X, Y, cv=5)
print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

## Alternative way to cross-validation
# scaler = preprocessing.StandardScaler().fit(X_train)
# X_train_transformed = scaler.transform(X_train)
# clf = svm.SVC(C=1).fit(X_train_transformed, Y_train)
# X_test_transformed = scaler.transform(X_test)
# print ("Accuracy %0.2f ",clf.score(X_test_transformed, Y_test))
