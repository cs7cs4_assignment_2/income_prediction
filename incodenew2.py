# -*- coding: utf-8 -*-
"""
Created on Tue Nov 19 12:25:31 2019

@author: rajaredy
"""

import pandas as pd
import numpy as np
#from sklearn import linear_model
#from sklearn.model_selection import cross_val_score, GridSearchCV
from sklearn.ensemble import RandomForestRegressor
#from sklearn.preprocessing import MinMaxScaler

#Load Train Data
na_val= ['#N/A','nA','#NA','nA','#NUM!']
df_tr = pd.read_csv("Train.csv",na_values=na_val)
df_tr = df_tr.drop_duplicates()                                           
df_tr = df_tr.drop(['Instance','Wears Glasses','Hair Color','Body Height [cm]'],axis=1)

#Load Test Data
df_ts = pd.read_csv("Test.csv",na_values=na_val)
df_ts = df_ts.drop(['Instance','Wears Glasses','Hair Color','Body Height [cm]'],axis=1)


df_tr = df_tr.rename(columns={'Total Yearly Income [EUR]':'Income'})
df_ts = df_ts.rename(columns={'Total Yearly Income [EUR]':'Income'})

cats = [col for col in df_tr.columns if df_tr[col].dtypes == 'object']
cons = list(df_tr.columns[~df_tr.columns.isin(cats)])

cats_na = [x for x in cats if df_tr[x].isna().any()]
cons_na = [x for x in cons if df_tr[x].isna().any()]

# Fill missing values
from sklearn.impute import SimpleImputer
simpleimputermedian=SimpleImputer(strategy='mean')
for x in cons_na:
    df_tr[x]=simpleimputermedian.fit_transform(df_tr[x].values.reshape(-1,1))
    
for x in cats_na:
    df_tr[x] = df_tr[x].fillna('missing')

trainX = df_tr.iloc[:,:-1]
trainy = df_tr.iloc[:,-1]

#Target Encoding
def cust_target_encode(df,cat_var,tgt_var,sm=300):
    dict_smooth = dict()
    #dict_gmean = dict()
    for col in cat_var:        
        #calculate the number of values and the mean of each group
        agg = df.groupby(col)[tgt_var].agg(['count','mean'])
        agg_count = agg['count']
        agg_mean = agg['mean']
        
        #calculate smooth_mean
        smooth = (agg_count * agg_mean + sm * agg_mean) / (agg_count + sm)
        df[col] =  df[col].map(smooth)
        dict_smooth[col] = smooth
    return df,dict_smooth


#Apply target encoding on training data
train_ctgt,test_smooth = cust_target_encode(df_tr,cats,'Income')


#Fix the missing data on test data

cats = [col for col in df_ts.columns if df_ts[col].dtypes == 'object']
cons = list(df_ts.columns[~df_ts.columns.isin(cats)])

cats_na = [x for x in cats if df_ts[x].isna().any()]
cons_na = [x for x in cons if df_ts[x].isna().any()]

for x in cons_na[:-1]:
    df_ts[x]=simpleimputermedian.transform(df_ts[x].values.reshape(-1,1))

for x in cats_na:
    df_ts[x] = df_ts[x].fillna('missing')      

for c in [x for x in df_ts.columns if df_ts[x].dtypes == 'object']:
    df_ts.loc[:,c] = df_ts[c].map(test_smooth[c])


#Fill missing values
com = [x for x in cats if df_ts[x].isna().any()]
for x in com:
    df_ts[x]=simpleimputermedian.transform(df_ts[x].values.reshape(-1,1))
    
test_data = df_ts.iloc[:,:-1]
  

#Build model

from sklearn.model_selection import train_test_split
Xtrain, Xtest, Ytrain, Ytest = train_test_split(train_ctgt.iloc[:,:-1], train_ctgt.iloc[:,-1], test_size=0.2, random_state=0)

import lightgbm as lgb
data_train = lgb.Dataset(Xtrain, label=Ytrain)
data_val = lgb.Dataset(Xtest, label=Ytest)
params = {}
params = {
    'boosting_type': 'gbdt',
    'objective': 'regression',
    'metric': 'mae',
    'learning_rate': 0.001,
    'num_leaves': 31,
    'feature_fraction': 0.9,
    'bagging_fraction': 0.8,
    'bagging_freq': 5,
    'verbose': 0
}

clf = lgb.train(params, data_train, 30000, valid_sets = [data_train, data_val], verbose_eval=1000, early_stopping_rounds=500)


data_pred = clf.predict(df_ts)

df_exp = pd.DataFrame()
test = pd.read_csv('Test.csv')
df_exp['Instance'] = test['Instance']
df_exp['Total Yearly Income [EUR]'] = data_pred
df_exp.to_csv(r'output12345.csv',index=False) 