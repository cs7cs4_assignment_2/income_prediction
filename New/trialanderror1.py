# -*- coding: utf-8 -*-
"""
Created on Mon Nov 25 14:41:03 2019

@author: rajaredy
"""

import pandas as pd
import numpy as np
#from sklearn import linear_model
#from sklearn.model_selection import cross_val_score, GridSearchCV
from sklearn.ensemble import RandomForestRegressor
import matplotlib.pyplot as plt
#from sklearn.preprocessing import MinMaxScaler

#Load Train Data
na_val= ['#N/A','nA','#NA','nA','#NUM!','unknown']
df_tr = pd.read_csv("Train.csv",na_values=na_val)
df_tr = df_tr.drop_duplicates()                                           
df_tr = df_tr.drop(['Instance','Wears Glasses','Hair Color','Crime Level in the City of Employement','Body Height [cm]'],axis=1)

#Load Test Data
df_ts = pd.read_csv("Test.csv",na_values=na_val)
df_ts = df_ts.drop(['Instance','Wears Glasses','Hair Color','Crime Level in the City of Employement','Body Height [cm]'],axis=1)

#Prune the profession column 

df_tr['Profession'] = df_tr['Profession'].str.replace(" ","").str[:5].str.strip()
df_ts['Profession'] = df_ts['Profession'].str.replace(" ","").str[:5].str.strip()

df_tr['Housing Situation'] = df_tr['Housing Situation'].str.split(" ", n = 1, expand = True)[0]
df_ts['Housing Situation'] = df_ts['Housing Situation'].str.split(" ", n = 1, expand = True)[0]

df_tr = df_tr.rename(columns={'Total Yearly Income [EUR]':'Income'})
df_ts = df_ts.rename(columns={'Total Yearly Income [EUR]':'Income'})
df_tr = df_tr.rename(columns={'Work Experience in Current Job [years]':'Work Exp'})
df_ts = df_ts.rename(columns={'Work Experience in Current Job [years]':'Work Exp'})
#df_tr = df_tr.rename(columns={'Body Height [cm]':'Height'})
#df_ts = df_ts.rename(columns={'Body Height [cm]':'Height'})
df_tr = df_tr.rename(columns={'Yearly Income in addition to Salary (e.g. Rental Income)':'Additional Sal'})
df_ts = df_ts.rename(columns={'Yearly Income in addition to Salary (e.g. Rental Income)':'Additional Sal'})

df_tr['Additional Sal'] = df_tr['Additional Sal'].str.split(" ", n = 1, expand = True)[0]
df_tr['Additional Sal'] = df_tr['Additional Sal'].astype(float)
df_ts['Additional Sal'] = df_ts['Additional Sal'].str.split(" ", n = 1, expand = True)[0]
df_ts['Additional Sal'] = df_ts['Additional Sal'].astype(float)


df_ts = df_ts.iloc[:,:-1]

#preprocess traing data


cats_tr = [col for col in df_tr.columns if df_tr[col].dtypes == 'object']
cons_tr = list(df_tr.columns[~df_tr.columns.isin(cats_tr)])

cats_na_tr = [x for x in cats_tr if df_tr[x].isna().any()]
cons_na_tr = [x for x in cons_tr if df_tr[x].isna().any()]

from sklearn.impute import SimpleImputer
simpleimputer=SimpleImputer(strategy='mean')
for x in cons_na_tr:
    df_tr[x]=simpleimputer.fit_transform(df_tr[x].values.reshape(-1,1))
    
    
for x in cats_na_tr:
    df_tr[x] = df_tr[x].fillna('missing')

# preprcess test data
    
cats_ts = [col for col in df_ts.columns if df_ts[col].dtypes == 'object']
cons_ts = list(df_ts.columns[~df_ts.columns.isin(cats_ts)])

cats_na_ts = [x for x in cats_ts if df_ts[x].isna().any()]
cons_na_ts = [x for x in cons_ts if df_ts[x].isna().any()]


for x in cons_na_ts:
    df_ts[x]=simpleimputer.fit_transform(df_ts[x].values.reshape(-1,1))
    

for x in cats_na_ts:
    df_ts[x] = df_ts[x].fillna('missing')   
    
#concatenate training and test data for target encoding
    
trainX = df_tr.iloc[:,:-1]
trainy = df_tr.iloc[:,-1]

#Target Encoding
from category_encoders import TargetEncoder
tgtE = TargetEncoder()
tgtE.fit(trainX,trainy)
trainX = tgtE.transform(trainX)

test_data = tgtE.transform(df_ts)

x = pd.concat([trainX,trainy],axis=1)
mn = x.corr()

plt.scatter(df_tr['Profession'], df_tr['Income'])
plt.show()



plt.scatter(df_tr['Satisfation with employer'], df_tr['Income'])
plt.show()


#Build model

from sklearn.model_selection import train_test_split
Xtrain, Xtest, Ytrain, Ytest = train_test_split(trainX, trainy, test_size=0.3, random_state=0)




import lightgbm as lgb
data_train = lgb.Dataset(Xtrain, label=Ytrain)
data_val = lgb.Dataset(Xtest, label=Ytest)
params = {}
params = {
    'boosting_type': 'gbdt',
    'objective': 'regression',
    'metric': 'mae',
    'learning_rate': 0.02,
    'num_leaves': 31,
    #'feature_fraction': 0.9,
    #'bagging_fraction': 0.8,
    #'bagging_freq': 5,
    'verbose': 0
}

clf = lgb.train(params, data_train, 30000, valid_sets = [data_train, data_val], verbose_eval=1000, early_stopping_rounds=500)


data_pred = clf.predict(test_data)

df_exp = pd.DataFrame()
test = pd.read_csv('Test.csv')
df_exp['Instance'] = test['Instance']
df_exp['Total Yearly Income [EUR]'] = data_pred
df_exp.to_csv(r'output_.0515.csv',index=False) 