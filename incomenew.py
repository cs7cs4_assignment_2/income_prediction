# -*- coding: utf-8 -*-
"""
Created on Mon Nov 18 15:12:11 2019

@author: rajaredy
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Nov 18 15:01:51 2019

@author: rajaredy
"""

import pandas as pd
import numpy as np
#from sklearn import linear_model
#from sklearn.model_selection import cross_val_score, GridSearchCV
from sklearn.ensemble import RandomForestRegressor
#from sklearn.preprocessing import MinMaxScaler

#Load Train Data
na_val= ['#N/A','nA','#NA','nA','#NUM!']
df_tr = pd.read_csv("Train.csv",na_values=na_val)
df_tr = df_tr.drop_duplicates()                                           
df_tr = df_tr.drop(['Instance','Wears Glasses','Hair Color','Body Height [cm]'],axis=1)

#Load Test Data
df_ts = pd.read_csv("Test.csv",na_values=na_val)
df_ts = df_ts.drop(['Instance','Wears Glasses','Hair Color','Body Height [cm]'],axis=1)


#Rename columns


d_rename={}
for col_name in list(df_tr.columns):
    d_rename[col_name] = col_name.split()[0]


df_tr = df_tr.rename(columns={'Total Yearly Income [EUR]':'Income'})
df_ts = df_ts.rename(columns={'Total Yearly Income [EUR]':'Income'})

cats = [col for col in df_tr.columns if df_tr[col].dtypes == 'object']
cons = list(df_tr.columns[~df_tr.columns.isin(cats)])

cats_na = [x for x in cats if df_tr[x].isna().any()]
cons_na = [x for x in cons if df_tr[x].isna().any()]


from sklearn.impute import SimpleImputer
simpleimputermedian=SimpleImputer(strategy='mean')

for x in cons_na:
    df_tr[x]=simpleimputermedian.fit_transform(df_tr[x].values.reshape(-1,1))
    
for x in cats_na:
    df_tr[x] = df_tr[x].fillna('missing')


trainX = df_tr.iloc[:,:-1]
trainy = df_tr.iloc[:,-1]

#Custom Target Encoder

def cust_Target_Encode(df,cat_var,tgt_var,sm=300):
    dict_smooth = dict()
    #dict_gmean = dict()
    for col in cat_var:        
        #calculate the number of values and the mean of each group
        agg = df.groupby(col)[tgt_var].agg(['count','mean'])
        agg_count = agg['count']
        agg_mean = agg['mean']
        
        #calculate smooth_mean
        smooth = (agg_count * agg_mean + sm * agg_mean) / (agg_count + sm)
        df[col] =  df[col].map(smooth)
        dict_smooth[col] = smooth
    return df,dict_smooth
    
   

#Target Encoding
from category_encoders import TargetEncoder
tgtE = TargetEncoder()
tgtE.fit(trainX,trainy)
trainX = tgtE.transform(trainX)

#Custom_taget call
train_ctgt,test_smooth = cust_Target_Encode(df_tr,cats,'Income')


#fit to test data
cats = [col for col in df_ts.columns if df_ts[col].dtypes == 'object']
cons = list(df_ts.columns[~df_ts.columns.isin(cats)])

cats_na = [x for x in cats if df_ts[x].isna().any()]
cons_na = [x for x in cons if df_ts[x].isna().any()]



for x in cons_na[:-1]:
    df_ts[x]=simpleimputermedian.transform(df_ts[x].values.reshape(-1,1))


test_data = tgtE.transform(df_ts.iloc[:,:-1])


#Build the model
rfr = RandomForestRegressor(n_estimators=250,n_jobs=-1)
rfr.fit(trainX, trainy)

df_pred = rfr.predict(test_data)

#LGB
from sklearn.model_selection import train_test_split
Xtrain, Xtest, Ytrain, Ytest = train_test_split(trainX, trainy, test_size=0.2, random_state=0)

#Test
from sklearn.model_selection import train_test_split
Xtrain, Xtest, Ytrain, Ytest = train_test_split(train_ctgt.iloc[:,:-1], train_ctgt.iloc[:,-1], test_size=0.2, random_state=0)

import lightgbm as lgb
data_train = lgb.Dataset(Xtrain, label=Ytrain)
data_val = lgb.Dataset(Xtest, label=Ytest)
params = {}
params = {
    'boosting_type': 'gbdt',
    'objective': 'regression',
    'metric': 'mae',
    'learning_rate': 0.001,
    'num_leaves': 31,
    'feature_fraction': 0.9,
    'bagging_fraction': 0.8,
    'bagging_freq': 5,
    'verbose': 0
}

clf = lgb.train(params, data_train, 30000, valid_sets = [data_train, data_val], verbose_eval=1000, early_stopping_rounds=500)


#Custom Target Encoding technique to test data

for c in [x for x in df_ts.columns if df_ts[x].dtypes == 'object']:
    df_ts.loc[:,c] = df_ts[c].map(test_smooth[c])
    
#Fill missing values
def fillMissingValue(X):
    d = {}
    for c in list(X.columns):
        if X[c].dtypes == 'object':
            d[c] = 'missing'
        else:
            d[c] = X[c].mean()
    return X.fillna(value=d)

df_ts = fillMissingValue(df_ts)




clf_pred = clf.predict(test_data)

df_exp = pd.DataFrame()
test = pd.read_csv('Test.csv')
df_exp['Instance'] = test['Instance']
df_exp['Total Yearly Income [EUR]'] = df_pred
df_exp.to_csv(r'output1234.csv',index=False) 





'''
for x in cons:
    datasetTrain[x]=simpleimputermedian.fit_transform(datasetTrain[x].values.reshape(-1,1))

def fillMissingValue(X):
    d = {}
    for c in list(X.columns):
        if X[c].dtypes == 'object':
            d[c] = 'NA' #X[c].value_counts().index[0]
        else:
            d[c] = X[c].mean()
    return X.fillna(value=d)


df_tr = fillMissingValue(df_tr)
df_ts = fillMissingValue(df_ts)


def calc_smooth_mean(df,cat_var,tgt_var,sm=300):
    dict_smooth = dict()
    #dict_gmean = dict()
    for col in cat_var:
        g_mean = df[tgt_var].mean()
        
        #calculate the number of values and the mean of each group
        agg = df.groupby(col)[tgt_var].agg(['count','mean'])
        agg_count = agg['count']
        agg_mean = agg['mean']
        
        #calculate smooth_mean
        smooth = (agg_count * agg_mean + sm * agg_mean) / (agg_count + sm)
        #print(smooth)
        df[col] =  df[col].map(smooth)
        #dict_gmean[col] = g_mean
        dict_smooth[col] = smooth
    return df,dict_smooth


df_tr,test_smooth = calc_smooth_mean(df_tr,[x for x in df_tr.columns if df_tr[x].dtypes == 'object'],'Income')


for c in [x for x in df_ts.columns if df_ts[x].dtypes == 'object']:
    df_ts.loc[:,c] = df_ts[c].map(test_smooth[c])
    
    
#Changing dataframe to numpy also works with dataframe
X_train = df_tr.iloc[:,:-1].values
y_train =  df_tr.iloc[:,-1].values
#numpy dataframe for test data
X_test = df_ts.iloc[:,:-1]
X_test = fillMissingValue(X_test).values


rfr = RandomForestRegressor(n_estimators=250, criterion='mae', max_depth=10)
rfr.fit(X_train, y_train)


#Predict the outcome
y_pred = rfr.predict(X_test)


df_exp = pd.DataFrame()
test = pd.read_csv('Test.csv')
df_exp['Instance'] = test['Instance']
df_exp['Total Yearly Income [EUR]'] = y_pred
df_exp.to_csv(r'output12.csv',index=False) 

'''