# -*- coding: utf-8 -*-
"""
Created on Sat Nov 16 19:08:04 2019

@author: yesh2
"""

#import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split 
from sklearn.linear_model import BayesianRidge
from sklearn.linear_model import LinearRegression
from sklearn import metrics
from scipy import stats
from sklearn.ensemble import RandomForestRegressor,GradientBoostingRegressor
from sklearn.ensemble import AdaBoostRegressor
from sklearn import linear_model
from sklearn.linear_model import SGDRegressor
from category_encoders import TargetEncoder
from sklearn.feature_selection import SelectKBest, f_regression
import xgboost
from xgboost import XGBRegressor
 

datasetTrain=pd.read_csv('Train.csv',na_values=['#N/A','nA'])
datasetTrain = datasetTrain.drop_duplicates()

datasetTest = pd.read_csv('Test.csv',na_values=['#NA','nA','#NUM!'])
datasetTest = datasetTest.iloc[:,:-1]

datasetTrain = datasetTrain.rename(columns={'Total Yearly Income [EUR]':'Income'})
#col_na = dict(dataset.isnull().any())

#col_naa = ['Gender','Hair Color','Housing Situation','Profession','Satisfation with employer','University Degree','Year of Record']
#na_col = dataset[dataset[col_naa]].columns

cats = [col for col in datasetTrain.columns if datasetTrain[col].dtypes == 'object']
cons = list(datasetTrain.columns[~datasetTrain.columns.isin(cats)])

cats_na = [x for x in cats if datasetTrain[x].isna().any()]
cons_na = [x for x in cons if datasetTrain[x].isna().any()]



from sklearn.impute import SimpleImputer
simpleimputermedian=SimpleImputer(strategy='median')

for x in cons:
    datasetTrain[x]=simpleimputermedian.fit_transform(datasetTrain[x].values.reshape(-1,1))
    

trainX = datasetTrain.iloc[:,:-1]
trainy = datasetTrain.iloc[:,-1]


#Target Encoding
tgtE = TargetEncoder()
tgtE.fit(trainX,trainy)
trainX = tgtE.transform(trainX)

#Auto feature Selection
KBest= SelectKBest(f_regression, k=15)
KBest.fit(trainX,trainy)
X=KBest.transform(trainX)

Xtrain, Xtest, Ytrain, Ytest = train_test_split(X, trainy, test_size=0.2, random_state=0)

#Building model


regressor = GradientBoostingRegressor(n_estimators=1000)
fitResult = regressor.fit(Xtrain, Ytrain)
YPredTest = regressor.predict(Xtest)

###lgbm
import lightgbm as lgb

data_train = lgb.Dataset(Xtrain, label=Ytrain)
data_val = lgb.Dataset(Xtest, label=Ytest)
params = {}
params = {
    'boosting_type': 'gbdt',
    'objective': 'regression',
    'metric': 'mae',
    'learning_rate': 0.001,
    'num_leaves': 31,
    'feature_fraction': 0.9,
    'bagging_fraction': 0.8,
    'bagging_freq': 5,
    'verbose': 0
}

clf = lgb.train(params, data_train, 100000, valid_sets = [data_train, data_val], verbose_eval=1000, early_stopping_rounds=500)


cats = [col for col in datasetTest.columns if datasetTest[col].dtypes == 'object']
cons = list(datasetTest.columns[~datasetTest.columns.isin(cats)])

cats_na = [x for x in cats if datasetTest[x].isna().any()]
cons_na = [x for x in cons if datasetTest[x].isna().any()]

#Apply median stratergy fir test data
for x in cons:
    datasetTest[x]=simpleimputermedian.transform(datasetTest[x].values.reshape(-1,1))


datasetTest1 = datasetTest.values
datasetTest2 = tgtE.transform(datasetTest)
datasetTest3 = KBest.transform(datasetTest2)
predict_lgb = regressor.predict(datasetTest3)

df_exp = pd.DataFrame()
df_exp['Instance'] = datasetTest['Instance']
df_exp['Total Yearly Income [EUR]'] = predict_lgb
df_exp.to_csv(r'output.csv',index=False) 


#calcuate Accuracy
from sklearn.metrics import mean_absolute_error
mae = mean_absolute_error(Ytest, YPredTest)

#-------------------------------------------------------------------
#Scaling and plotting


print("Before scaling")
for i in list(trainX.columns):
    X = trainX[i]
    y = trainy
    plt.scatter(X, y)
    #plt.plot(X, y, color = 'blue')
    plt.title('Income')
    plt.xlabel(i)
    plt.ylabel('Income')
    plt.show()



#second plot
from sklearn.preprocessing import QuantileTransformer
from sklearn.preprocessing import RobustScaler
from sklearn.preprocessing import PowerTransformer
from sklearn.preprocessing import minmax_scale

data_corrX = trainX
col_train = list(data_corrX.columns)
data_corrY = trainy
data_corrY = minmax_scale(data_corrY)
data_corrX = PowerTransformer(method='yeo-johnson').fit_transform(data_corrX)

for i in range(16):
    X = data_corrX[:,i]
    y = data_corrY
    plt.scatter(X, y)
    #plt.plot(X, y, color = 'blue')
    plt.title('Income')
    plt.xlabel(col_train[i])
    plt.ylabel('Income')
    plt.show()

#data_corr = np.concatenate([data_corrX,data_corrY],axis=1)
#datacorr =  data_corr.corr()
#----------
    
#Auto feature Selection
KBest= SelectKBest(f_regression, k=5)
KBest.fit(data_corrX,trainy)
X=KBest.transform(data_corrX)

Xtrain, Xtest, Ytrain, Ytest = train_test_split(X, trainy, test_size=0.2, random_state=0)

import lightgbm as lgb

data_train = lgb.Dataset(Xtrain, label=Ytrain)
data_val = lgb.Dataset(Xtest, label=Ytest)
params = {}
params = {
    'boosting_type': 'gbdt',
    'objective': 'regression',
    'metric': 'mae',
    'learning_rate': 0.001,
    'num_leaves': 31,
    'feature_fraction': 0.9,
    'bagging_fraction': 0.8,
    'bagging_freq': 5,
    'verbose': 0
}

clf = lgb.train(params, data_train, 100000, valid_sets = [data_train, data_val], verbose_eval=1000, early_stopping_rounds=500)



#Second Plot






'''
import lightgbm as lgb

data_train = lgb.Dataset(Xtrain, label=Ytrain)
data_val = lgb.Dataset(Xtest, label=Ytest)
params = {}
params = {
    'boosting_type': 'gbdt',
    'objective': 'regression',
    'metric': 'mae',
    'learning_rate': 0.001,
    'num_leaves': 31,
    'feature_fraction': 0.9,
    'bagging_fraction': 0.8,
    'bagging_freq': 5,
    'verbose': 0
}

clf = lgb.train(params, data_train, 100000, valid_sets = [data_train, data_val], verbose_eval=1000, early_stopping_rounds=500)



trn_data = lgb.Dataset(x_train, label=y_train)
val_data = lgb.Dataset(x_val, label=y_val)
# test_data = lgb.Dataset(X_test)
clf = lgb.train(params, trn_data, 100000, valid_sets = [trn_data, val_data], verbose_eval=1000, early_stopping_rounds=500)
pre_test_lgb = clf.predict(X_test)
'done'
clf = lgb.train(params, d_train, 1000)


data_corr = pd.concat([trainX,trainy],axis=1,sort=False)
datacorr =  data_corr.corr()

for i in list(data_corr.columns)[0:-1]:
    X = data_corr[i]
    y = data_corr['Income']
    plt.scatter(X, y)
    #plt.plot(X, y, color = 'blue')
    plt.title('Income')
    plt.xlabel(i)
    plt.ylabel('Income')
    plt.show()
    
'''